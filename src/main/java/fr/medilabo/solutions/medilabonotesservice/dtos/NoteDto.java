package fr.medilabo.solutions.medilabonotesservice.dtos;

import java.time.LocalDateTime;

public class NoteDto {

    private String _id;

    private int patientId;

    private String doctorName;

    private LocalDateTime postDateTime;

    private String message;

    public NoteDto() {
        // Empty Constructor
    }

    public NoteDto(String _id, int patientId, String doctorName, LocalDateTime postDateTime, String message) {
        this._id = _id;
        this.patientId = patientId;
        this.doctorName = doctorName;
        this.postDateTime = postDateTime;
        this.message = message;
    }

    public String get_id() {
        return this._id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getPatientId() {
        return this.patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getDoctorName() {
        return this.doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public LocalDateTime getPostDateTime() {
        return this.postDateTime;
    }

    public void setPostDateTime(LocalDateTime postDateTime) {
        this.postDateTime = postDateTime;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
