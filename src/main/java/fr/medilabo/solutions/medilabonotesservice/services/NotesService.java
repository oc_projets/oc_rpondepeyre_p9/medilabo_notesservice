package fr.medilabo.solutions.medilabonotesservice.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.medilabo.solutions.medilabonotesservice.config.BadRequestException;
import fr.medilabo.solutions.medilabonotesservice.dtos.NoteDto;
import fr.medilabo.solutions.medilabonotesservice.models.Note;
import fr.medilabo.solutions.medilabonotesservice.repositories.NoteRepository;

@Service
public class NotesService {

    private final NoteRepository repository;

    public NotesService(NoteRepository noteRepository) {
        this.repository = noteRepository;
    }

    public Note findById(String id) throws BadRequestException {
        Optional<Note> noteBox = this.repository.findById(id);
        if (noteBox.isPresent()) {
            return noteBox.get();
        } else {
            throw new BadRequestException("No note founded with this ID");
        }
    }

    public List<Note> findByPatientId(int patientId) {
        return this.repository.findByPatientId(patientId);
    }

    @Transactional
    public void postNote(Note note) throws BadRequestException {
        if (note.get_id() == null) {
            this.repository.save(note);
        } else {
            throw new BadRequestException("You can't post a new note with a predefined identifier");
        }
    }

    @Transactional
    public void editNote(Note note) throws BadRequestException {
        if (note.get_id() != null) {
            this.repository.save(note);
        } else {
            throw new BadRequestException("You must specify note identifier to perfom this update");
        }
    }

    @Transactional
    public void deleteNote(String id) throws BadRequestException {
        Note note = this.findById(id);
        this.repository.delete(note);
    }

    public Note mapToNote(NoteDto dto) {
        return new Note(dto.get_id(), dto.getPatientId(), dto.getDoctorName(), dto.getPostDateTime(), dto.getMessage());
    }
}
