package fr.medilabo.solutions.medilabonotesservice.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.medilabo.solutions.medilabonotesservice.models.Note;

public interface NoteRepository extends MongoRepository<Note, String> {

    public List<Note> findByPatientId(int patientId);
}
