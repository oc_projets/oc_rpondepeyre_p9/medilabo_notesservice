package fr.medilabo.solutions.medilabonotesservice.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.medilabo.solutions.medilabonotesservice.config.BadRequestException;
import fr.medilabo.solutions.medilabonotesservice.dtos.NoteDto;
import fr.medilabo.solutions.medilabonotesservice.models.Note;
import fr.medilabo.solutions.medilabonotesservice.services.NotesService;

@RestController
@RequestMapping("/note")
public class NoteController {

    private final NotesService service;

    public NoteController(NotesService service) {
        this.service = service;
    }

    @GetMapping("all")
    public ResponseEntity<List<Note>> findByPatient(@RequestParam int patientId) {
        return new ResponseEntity<>(this.service.findByPatientId(patientId), HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<Note> getNote(@RequestParam String id) throws BadRequestException {
        return new ResponseEntity<>(this.service.findById(id), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<String> postNote(@RequestBody NoteDto note) throws BadRequestException {
        this.service.postNote(service.mapToNote(note));
        return new ResponseEntity<>("{\"message\": \"Note successfully posted\"}", HttpStatus.CREATED);
    }

    @PutMapping("")
    public ResponseEntity<String> editNote(@RequestBody NoteDto note) throws BadRequestException {
        this.service.editNote(service.mapToNote(note));
        return new ResponseEntity<>("{\"message\": \"Note successfully modified\"}", HttpStatus.CREATED);
    }

    @DeleteMapping("")
    public ResponseEntity<String> deleteNote(@RequestParam String id) throws BadRequestException {
        this.service.deleteNote(id);
        return new ResponseEntity<>("{\"message\": \"Note successfully modified\"}", HttpStatus.CREATED);
    }
}
