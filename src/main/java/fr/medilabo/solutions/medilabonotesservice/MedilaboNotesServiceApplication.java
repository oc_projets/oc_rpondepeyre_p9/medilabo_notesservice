package fr.medilabo.solutions.medilabonotesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "fr.medilabo.solutions.medilabonotesservice.repositories")
public class MedilaboNotesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedilaboNotesServiceApplication.class, args);
	}

}
