package fr.medilabo.solutions.MedilaboNotesService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fr.medilabo.solutions.medilabonotesservice.config.BadRequestException;
import fr.medilabo.solutions.medilabonotesservice.controllers.NoteController;
import fr.medilabo.solutions.medilabonotesservice.dtos.NoteDto;
import fr.medilabo.solutions.medilabonotesservice.models.Note;
import fr.medilabo.solutions.medilabonotesservice.services.NotesService;

@ExtendWith(MockitoExtension.class)
public class NoteControllerTest {

    @Mock
    NotesService service;
    @InjectMocks
    NoteController controller;

    @Test
    public void findByPatientId() {
        when(service.findByPatientId(1)).thenReturn(new ArrayList<>());
        ResponseEntity<List<Note>> result = this.controller.findByPatient(1);
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertNotNull(result.getBody());
    }

    @Test
    public void getNote() throws BadRequestException {
        when(service.findById("id")).thenReturn(new Note());
        ResponseEntity<Note> result = this.controller.getNote("id");
        assertEquals(result.getStatusCode(), HttpStatus.OK);
        assertNotNull(result.getBody());
    }

    @Test
    public void postNote() throws BadRequestException {
        NoteDto note = new NoteDto();
        doNothing().when(service).postNote(any());
        ResponseEntity<String> result = this.controller.postNote(note);
        verify(service).mapToNote(note);
        assertEquals(result.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void editNote() throws BadRequestException {
        NoteDto note = new NoteDto();
        doNothing().when(service).editNote(any());
        ResponseEntity<String> result = this.controller.editNote(note);
        verify(service).mapToNote(note);
        assertEquals(result.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void deletePatient() throws BadRequestException {
        doNothing().when(service).deleteNote("id");
        ResponseEntity<String> result = this.controller.deleteNote("id");
        assertEquals(result.getStatusCode(), HttpStatus.CREATED);
    }
}
