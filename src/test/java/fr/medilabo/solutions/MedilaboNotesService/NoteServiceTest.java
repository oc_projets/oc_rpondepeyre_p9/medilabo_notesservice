package fr.medilabo.solutions.MedilaboNotesService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.medilabo.solutions.medilabonotesservice.config.BadRequestException;
import fr.medilabo.solutions.medilabonotesservice.dtos.NoteDto;
import fr.medilabo.solutions.medilabonotesservice.models.Note;
import fr.medilabo.solutions.medilabonotesservice.repositories.NoteRepository;
import fr.medilabo.solutions.medilabonotesservice.services.NotesService;

@ExtendWith(MockitoExtension.class)
public class NoteServiceTest {

    @Mock
    NoteRepository repository;

    @InjectMocks
    NotesService service;

    @Test
    public void findByPatientId() {
        when(repository.findByPatientId(0)).thenReturn(new ArrayList<>());
        List<Note> result = service.findByPatientId(0);
        verify(repository).findByPatientId(0);
        assertNotNull(result);
    }

    @Test
    public void findById_Ok() throws BadRequestException {
        when(repository.findById("id")).thenReturn(Optional.of(new Note()));
        Note result = this.service.findById("id");
        verify(repository).findById("id");
        assertNotNull(result);
    }

    @Test
    public void findById_Ko() {
        when(repository.findById("id")).thenReturn(Optional.empty());
        assertThrows(BadRequestException.class, () -> {
            this.service.findById("id");
        }, "No note founded with this ID");
    }

    @Test
    public void postNote_Ok() throws BadRequestException {
        Note note = new Note(null, 1, "doctorName", LocalDateTime.now(), "message");
        this.service.postNote(note);
        verify(repository).save(note);
    }

    @Test
    public void postNote_Ko() {
        Note note = new Note("id", 1, "doctorName", LocalDateTime.now(), "message");
        assertThrows(BadRequestException.class, () -> {
            this.service.postNote(note);
        }, "You can't post a new note with a predefined identifier");

    }

    @Test
    public void editNote_Ok() throws BadRequestException {
        Note note = new Note("id", 1, "doctorName", LocalDateTime.now(), "message");
        this.service.editNote(note);
        verify(repository).save(note);
    }

    @Test
    public void editNote_Ko() {
        Note note = new Note(null, 1, "doctorName", LocalDateTime.now(), "message");
        assertThrows(BadRequestException.class, () -> {
            this.service.editNote(note);
        }, "You must specify note identifier to perfom this update");

    }

    @Test
    public void deleteNote_Ok() throws BadRequestException {
        when(repository.findById("id")).thenReturn(Optional.of(new Note()));
        this.service.deleteNote("id");
        verify(repository).findById("id");
        verify(repository).delete(any());
    }

    @Test
    public void deleteNote_Ko() {
        when(repository.findById("id")).thenReturn(Optional.empty());
        assertThrows(BadRequestException.class, () -> {
            this.service.deleteNote("id");
        }, "No note founded with this ID");
    }

    @Test
    public void mapToNotewithID() {
        LocalDateTime now = LocalDateTime.now();
        NoteDto noteDto = new NoteDto("abc", 1, "doctorName", now, "message");
        Note result = service.mapToNote(noteDto);
        assertEquals("abc", result.get_id());
        assertEquals(1, result.getPatientId());
        assertEquals("doctorName", result.getDoctorName());
        assertEquals(now, result.getPostDateTime());
        assertEquals("message", result.getMessage());
    }

    @Test
    public void mapToNotewithoutID() {
        LocalDateTime now = LocalDateTime.now();
        NoteDto noteDto = new NoteDto(null, 1, "doctorName", now, "message");
        Note result = service.mapToNote(noteDto);
        assertEquals(null, result.get_id());
        assertEquals(1, result.getPatientId());
        assertEquals("doctorName", result.getDoctorName());
        assertEquals(now, result.getPostDateTime());
        assertEquals("message", result.getMessage());
    }
}
